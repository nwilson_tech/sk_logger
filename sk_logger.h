/*
Copyright (C) 2016 Nathaniel Wilson

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.


Nathaniel Wilson        nwilson.tech@gmail.com
*/
#ifndef SK_LOGGER_H
#define SK_LOGGER_H
#include <iostream>    /// cout endl
#include <mutex>       /// mutex
#include <string>      /// string
#include <memory>      /// atomic functions
class sk_logger
{
public:
    sk_logger();
    virtual ~sk_logger();
    void write  (std::string msg  = "",
                 std::string res  = "",
                 std::string type = "dbg",
                 size_t log_max = 60);
};

#endif // SK_LOGGER_H
