/*
Copyright (C) 2016 Nathaniel Wilson

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.

Nathaniel Wilson        nwilson.tech@gmail.com
*/

#include "sk_logger.h"    // sk_logger header
#include <exception>      // std::exception
/// Create a static mutex for lock_guard in the log output
static std::mutex shared_mutex;
/// ctor
sk_logger::sk_logger()
{
    /// Empty Constructor
}
/// dtor
sk_logger::~sk_logger()
{
    /// Empty Destructor
}


void sk_logger::write(std::string msg, std::string res, std::string type, size_t log_max)
{
   try{
       /// Lock Guard to allow correct usage with concurrency
       std::lock_guard<std::mutex> lock(shared_mutex);
       /// store message length for ease later
       size_t msg_size = msg.length();
       /// store results length for ease later
       size_t res_size = res.length();
       /// Declare seperators needed as a size_t and default to zero (0)
       /// for simplification later
       size_t sep_need = 0;
       /// Handle if the msg_size & res_size > log_max
       if ((msg_size+res_size)<=log_max)
       {
           /// Set seperators needed to the difference of log_max and
           /// the sum of message and result length
           sep_need = log_max - msg_size - res_size;
       }else
       {
           /// If message size greater than log_max
           /// throw range_error
           throw "range_error: inital message size exceeds log_max";
       }
       if (type.length()) /// if type has a value
       {
           /// Create a temporary string for length testing
           std::string t_string = "[" + type + "] ";
           /// Append to message string
           msg = t_string + msg;
           /// Test if seperators needed is not equal to zero
           /// additionally check if there are enough seperators
           /// availible to deduct from and maintian log_max
           if ((sep_need>0)&(sep_need>=t_string.length()))
           {
               /// Update sep_need to sep_need minus t_string.length()
               sep_need -= t_string.length();
           }
           else if ((msg.length()+res_size)>log_max)
           {
               /// If the new message size is greater than log_max
               /// throw range_error
               throw "range_error: resultant message size exceeds log_max";
           }
       }
       /// Finally output our results
       std::cout << msg << std::string(sep_need,'.') << res << std::endl;
   }
   catch (const char* error)
   {
      /// If a error occurs cerr the message
      std::cerr << "[logger] " << error << " (" << (msg.length()+res.length()) << ")" << '\n';
   }

}
